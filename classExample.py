# class Example:
#     number = 0
#     name = 'rakesh'
    
# def Main():
#     myself = Example()
    
#     myself.number = 1002
#     myself.name = 'rupesh'
    
#     print(myself.name + " " + str(myself.number))
    
# if __name__ == '__main__':
#     Main()    
    
    
    
class CreateTwoDimVector:
    x = 0.0
    y = 0.0
    
    def Set(self, x, y):
        self.x = x
        self.y = y


def Main():
    vec = CreateTwoDimVector()
    vec.Set(5,12)
    
    print(vec.x)
    print(vec.y)
    
if __name__ == '__main__':
    Main()  
    
class CreateDrink:
    # price = 0
    # color = ''
    def __init__(self, price , color , taste , weight , unit):
        self.price = price
        self.color = color
        self.taste = taste
        self.weight = weight
        self.unit = unit
        
    def my_taste(self):
        print(" My taste is " + self.taste)
            
tea = CreateDrink(13,'gry','sweet','100gms',1)
# juice = CreateDrink()
# coffee = CreateDrink()   

# tea.price = 10
# juice.price = 20
# coffee.price = 40


print(tea.my_taste())


class ShoppingList:
    product = []
    
    def __init__(self):
        print('my shopping list product')
        
    def add(self, name):
        self.product.append(name)
        
    def show(self):
        print(self.product)
     
groceries = ShoppingList()
groceries.add('Milk')
groceries.add('Vegitable')

print(groceries.show())

class Car:

    def __init__(self,pricee,colour,type):
        self.pricee=pricee
        self.colour=colour
        self.type=type
        
    def color(self): 
      print(" My Car color is " + self.colour) 
      
    def price(self): 
      print(" My Car cost is " , self.pricee)
      
    def shap(self): 
      print(" My Car type is " + self.type)       
        
        
car=Car(10000,'red','suv')    
       
print(car.color())
print(car.price())
print(car.shap())
                
    
                  